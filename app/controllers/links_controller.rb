class LinksController < ApplicationController
  before_action :find_link, only: %i[ show edit update destroy ]
  before_action :authenticate_user!, except: %i[ index ]

  def index
    @links = Link.all
  end

  def show
    @histories = User.joins(:link_user).where(:link_user =>{link_id: @link.id}).select("email, clicked")
  end

  def new
    @link = Link.new
  end

  def create
    @link = current_user.links.new(link_params)

    if @link.save
      redirect_to link_path(@link), notice: "Link was successfully created."
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @link.update(link_params)
      redirect_to link_path(@link), notice: "Link was successfully updated."
    else
      render :edit, status: :unprocessable_entity 
    end
  end

  def destroy
    @link.destroy

    redirect_to links_path, notice: "Link was successfully destroyed."
  end

  def short
    @link = Link.find_by_slug(params[:slug]) 

    if @link.nil?
      render file: "#{Rails.root}/public/404.html", layout: false,  status: 400
    elsif
      @link.update({clicked: @link.clicked + 1})

      if LinkUser.exists?(user_id: current_user.id, link_id: @link.id)
        @link_user = LinkUser.find_by(user_id: current_user.id, link_id: @link.id)
        @link_user.update({clicked: @link_user.clicked + 1})
      else
        LinkUser.create!(user_id: current_user.id, link_id: @link.id, clicked: 1)
      end

      redirect_to @link.url
    end
  end

  private
  def find_link
    @link = Link.find(params[:id])
  end

  def link_params
    params.require(:link).permit(:url, :slug)
  end
end
