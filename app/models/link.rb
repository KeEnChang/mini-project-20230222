class Link < ApplicationRecord
  include Rails.application.routes.url_helpers

  # validation
  validates :url, presence: true, uniqueness: true
  validates :slug, presence: true, uniqueness: true

  # relationships
  belongs_to :user
  has_many :link_user
  has_many :link_user_history, through: :link_user, source: :user

  # soft delete
  acts_as_paranoid

  # auto slug generation
  before_validation :generate_slug

  def generate_slug
    self.slug = SecureRandom.uuid[0..5] if self.slug.nil? || self.slug.empty?
  end

  def short
    # require 'uri'
    # uri = URI.parse(self.url)
    # uri = URI.parse("http://#{url}") if uri.scheme.nil?
    
    # return uri.scheme + "://" + uri.host.downcase + "/#{self.slug}"
    return "http://localhost:3000/#{self.slug}"
  end

end
