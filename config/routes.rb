Rails.application.routes.draw do
  devise_for :users
  resources :links
  
  get '/:slug', to: 'links#short', as: :short

  root 'links#index'
end
