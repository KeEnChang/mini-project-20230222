class AddClickedDefaultToLinks < ActiveRecord::Migration[6.1]
  def change
    change_column_default :links, :clicked, default: 0
  end
end
