class AddClickedDefault1ToLinks < ActiveRecord::Migration[6.1]
  def change
    change_column_default :links, :clicked, 0
  end
end
