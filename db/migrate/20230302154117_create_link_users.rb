class CreateLinkUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :link_users do |t|
      t.references :link, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.integer :clicked

      t.timestamps
    end
  end
end
