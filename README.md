# mini-project-20230222

## User Stories

- As an admin, I want to be able to log in to the shorten URL service so that I can create, modify, or delete short URLs.

- As an admin, I want to be able to create a new short URL by providing a full URL, so that visitors can access the full URL through the shortened version.

- As an admin, I want to be able to modify the full URL associated with a short URL, so that I can update the destination of the shortened link as needed.

- As an admin, I want to be able to delete a short URL, so that visitors can no longer access the associated full URL through the shortened version.

- As a visitor, I want to be able to enter a shortened URL and be redirected to the full URL associated with it, so that I can access the content I am looking for quickly and easily.

- As an admin, I want to be able to view the number of visits that each short URL has received, so that I can track usage and analyze traffic.

- As an admin, I want to be able to view the history of visits that each short URL has received, so that I can track usage and analyze traffic.

## What you are expcted to do

1. Create a rails project
2. Implement the features listed aove
3. Write unit tests for the code
4. You are NOT expected to implement URL redirect. You can simply show the full url on the returned page for visitors
